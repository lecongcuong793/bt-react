import React, { Component } from 'react';
import "./content.css"
import IphoneX  from"../../assets/img/sp_iphoneX.png"
import Note7  from"../../assets/img/sp_note7.png"
import Vivo  from"../../assets/img/sp_vivo850.png"
import Blackberry from"../../assets/img/sp_blackberry.png"

class Content extends Component {
    render() {
        return (
            <div className="content">
                <div>
                <h1 className="best">BEST SMARTPHONE</h1>
                <div className="list">
                    <div className="list__content">
                        <img src={IphoneX} alt="" />
                        <div className="list__item">
                            <h1>iPhone X</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="list__content">
                        <img src={Note7} alt="" />
                        <div className="list__item">
                            <h1>Galaxy Note7</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="list__content">
                        <img src={Vivo} alt="" />
                        <div className="list__item">
                            <h1>Vivo</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="list__content">
                        <img src={Blackberry} alt="" />
                        <div className="list__item">
                            <h1>Blackberry</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default Content;