import "./header.css"
import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div className="header">
               <div className="header__content">
                
                <div>
                    <h1>CyberSoft</h1>
                </div>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li ><a className="item" href="#">News</a></li>
                    <li ><a className="item" href="#">Products</a></li>
                    <li ><a className="item" href="#">Forum</a></li>
                </ul>
                
               </div>
            </div>
        );
    }
}

export default Header;