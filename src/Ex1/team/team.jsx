import React, { Component } from 'react';
import "./team.css"
import Macbook  from"../../assets/img/lt_macbook.png"
import Asus  from"../../assets/img/lt_rog.png"
import Hp  from"../../assets/img/lt_hp.png"
import Lenovo from"../../assets/img/lt_lenovo.png"

class Team extends Component {
    render() {
        return (
            <div className="team__container">
                <div>
                <h1 className="best__lp">BEST LAPTOP</h1>
                <div className="team">
                    <div className="team__content">
                        <img src={Macbook} alt="" />
                        <div className="team__item">
                            <h1>MACBOOK</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="team__content">
                        <img src={Asus} alt="" />
                        <div className="team__item">
                            <h1>ASUS ROG</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="team__content">
                        <img src={Hp} alt="" />
                        <div className="team__item">
                            <h1>HP OMEN</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                    <div className="team__content">
                        <img src={Lenovo} alt="" />
                        <div className="team__item">
                            <h1>LENOVO THINKPAD</h1>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi sequi nostrum numquam necessitatibus laboriosam soluta unde expedita illo minima esse.</p>
                            <button>Detail</button>
                            <button>Cart</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default Team;