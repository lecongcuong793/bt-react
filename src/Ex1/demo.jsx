import React, { Component } from 'react';
import Header from './header/header';
import Content from './content/content';
import Slide from './slide/slide';
import Team from './team/team';
import Footer from './footer/footer';


class Demo extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Slide />
                <Content/>
                <Team/>
                <Footer/>
            </div>
        );
    }
}

export default Demo;