import React, { Component } from 'react';
import "./footer.css"
import Promotion1 from "../../assets/img/promotion_1.png"
import Promotion2 from "../../assets/img/promotion_2.png"
import Promotion3 from "../../assets/img/promotion_3.jpg"

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <h1>PROMOTION</h1>
                <div className="footer__img">
                <img src={Promotion1} alt="" />
                <img src={Promotion2} alt="" />
                <img src={Promotion3} alt="" />
                </div>
            </div>
        );
    }
}

export default Footer;