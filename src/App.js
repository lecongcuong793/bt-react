import logo from "./logo.svg";
import "./App.css";
import Demo from "./Ex1/demo";
import Navbar from "./Ex2/navbar";

function App() {
  return (
    <div className="App">
      {/* <Demo /> */}
      <Navbar />
    </div>
  );
}

export default App;
