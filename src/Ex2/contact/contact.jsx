import React, { Component } from 'react';
import "./contact.css"
class Contact extends Component {
    render() {
        return (
            <div className="contact">
                <div className="contact__Do">
                    <h1>What We Do</h1>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis eligendi sed quo. Quaerat officiis esse exercitationem eum nihil,magnam iure ipsum! Est veniam, nemo ex praesentium deleniti corrupti repudiandae fugiat? Velit, quia dolores. Expedita, sit dicta nemo beatae explicabo corporis, ipsam alias sapiente aliquam, exercitationem cupiditate iure. <br></br> <br></br> Laboriosam nobis quod obcaecati tenetur quibusdam cumque cupiditate quo impedit quis perferendis  blanditiis quisquam corporis eaque deleniti ea consectetur odio voluptatum totam, vel eligendi veniam debitis reiciendis. Nobis sed temporibus excepturi iusto accusamus?</p>
                </div>
                <div className="contact__Us">
                    <h1>Contact US</h1>
                    <p>CyberSoft</p>
                    <p>Su Van Hanh, quận 10, Tp.HCM</p>
                    <p>website: cybersoft.edu.vn</p>
                </div>
            </div>
        );
    }
}

export default Contact;