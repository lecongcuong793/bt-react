import React, { Component } from 'react';
import "./title.css"
import Imgtitle from "../../assets/img/imgTeam.jpeg"

class Title extends Component {
    render() {
        return (
            <div className="title">
                <div className="title__content">
                    <img src={Imgtitle} alt="" />
                    <div className="title__item">
                        <h1>Card title</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est odio non ipsam? Deserunt fugit sapiente aliquam eius, aut voluptate sed!</p>
                        <button>Go somewhere</button>
                    </div>
                </div>
                <div className="title__content">
                    <img src={Imgtitle} alt="" />
                    <div className="title__item">
                        <h1>Card title</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est odio non ipsam? Deserunt fugit sapiente aliquam eius, aut voluptate sed!</p>
                        <button>Go somewhere</button>
                    </div>
                </div>
                <div className="title__content">
                    <img src={Imgtitle} alt="" />
                    <div className="title__item">
                        <h1>Card title</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est odio non ipsam? Deserunt fugit sapiente aliquam eius, aut voluptate sed!</p>
                        <button>Go somewhere</button>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default Title;