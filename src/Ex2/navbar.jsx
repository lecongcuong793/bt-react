import React, { Component } from "react";
import Header from "./header/header";
import Carousel from "./carousel/carousel";
import Contact from "./contact/contact";
import Title from "./title/title";
import Footer from "./footer/footer";

class Navbar extends Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <Contact />
        <Title />
        <Footer />
      </div>
    );
  }
}

export default Navbar;
